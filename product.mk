TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := arm

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-musleabihf/bin/arm-linux-musleabihf-
TARGET_UBOOT_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-
TARGET_LINUX_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-

TARGET_GLOBAL_CFLAGS += -march=armv6 -mfloat-abi=hard -mfpu=vfp
TARGET_DEFAULT_ARM_MODE = arm

TARGET_LINUX_IMAGE := uImage

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TOP_DIR)/trail/rpi3/config/

TARGET_UBI_IMAGE_OPTIONS := \
       --mkubifs="-m 1 -e 65408 -c 247 -x favor_lzo -F" \
       --ubinize="-p 0x10000 -m 1 $(TARGET_CONFIG_DIR)/ubinize.config"
